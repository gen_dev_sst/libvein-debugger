import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.2
import Vein 1.0 as Vein
import QmlToolBelt 1.0

Window {
  visible: true
  id: root
  width: 1280
  height: 1024
  color: "white"

  Vein.EntityModel {
    id: entModel
    src: "vein://192.168.6.142:12000/"
    //src: "vein://127.0.0.1:12000/"
  }

  SortFilterProxyModel {
    id: proxy
    source: entModel

    filterString: ".*" + searchField.text + ".*"
    filterSyntax: SortFilterProxyModel.RegExp
    filterCaseSensitivity: Qt.CaseInsensitive
  }

  /*
  roles[EntName] = "name";
  roles[EntModifiers] = "modifiers";
  roles[EntValue] = "value";
  roles[EntValueType] = "valuetype";
  roles[PeerName] = "peername";
  */

  Rectangle {
    id: searchBox
    color: "transparent"
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    height: 40

    Text {
      anchors.left: parent.left
      anchors.leftMargin: 8
      anchors.verticalCenter: parent.verticalCenter
      text: "Entities visible: " + entView.count
    }

    CheckBox {
      id: heatMap
      text: "Heatmap"

      anchors.verticalCenter: parent.verticalCenter
      anchors.right: searchField.left
      anchors.rightMargin: 12
    }

    TextField {
      id: searchField
      width: root.width/3
      placeholderText: qsTr("REGEX Search...")
      anchors.verticalCenter: parent.verticalCenter
      anchors.right: clearSearchField.left
      anchors.rightMargin: 2
    }
    Button {
      id: clearSearchField
      text: "x"
      onClicked: searchField.text="";
      height: parent.height/2.2
      width: height
      anchors.right: parent.right
      anchors.rightMargin: 8
      anchors.verticalCenter: parent.verticalCenter
    }
  }

  Row {
    id: headLine
    anchors.top: searchBox.bottom
    height:  30
    width: root.width
    spacing: 0
    Rectangle {
      color: "lightblue"
      height: parent.height
      width: parent.width/100*10
      border.width: 1
      border.color: "black"
      Text { text: "Peer name"; font.bold: true; anchors.fill: parent; anchors.margins: 4 }
    }
    Rectangle {
      color: "lightblue"
      height: parent.height
      width: parent.width/100*16
      border.width: 1
      border.color: "black"
      Text { text: "Entity name"; font.bold: true; anchors.fill: parent; anchors.margins: 4 }
    }
    Rectangle {
      color: "lightblue"
      height: parent.height
      width: parent.width/100*16
      border.width: 1
      border.color: "black"
      Text { text: "Modifiers"; font.bold: true; anchors.fill: parent; anchors.margins: 4 }
    }
    Rectangle {
      color: "lightblue"
      height: parent.height
      width: parent.width/100*7
      border.width: 1
      border.color: "black"
      Text { text: "Value type"; font.bold: true; anchors.fill: parent; anchors.margins: 4 }
    }
    Rectangle {
      color: "lightblue"
      height: parent.height
      width: parent.width/100*3
      border.width: 1
      border.color: "black"
      Text { text: "Size"; font.bold: true; anchors.fill: parent; anchors.margins: 4 }
    }
    Rectangle {
      color: "lightblue"
      height: parent.height
      width: root.width/100*48
      border.width: 1
      border.color: "black"
      Text { text: "Value"; font.bold: true; anchors.fill: parent; anchors.margins: 4 }
    }
  }

  ListView {
    id: entView
    clip: true
    anchors.fill: parent
    anchors.topMargin: searchBox.height+headLine.height
    model: proxy

    remove: Transition {
      ParallelAnimation {
        NumberAnimation { property: "opacity"; to: 0; duration: 300 }
        NumberAnimation { properties: "x"; to: 1000; duration: 300 }
      }
    }

    add: Transition {
      ParallelAnimation {
        NumberAnimation { property: "opacity"; to: 1; duration: 300 }
        NumberAnimation { properties: "x"; from: 1000; duration: 300 }
      }
    }
    //    move: Transition {
    //      NumberAnimation { properties: "y"; from: 0; duration: 300 }
    //    }

    delegate: Row {
      height:  (tEdit.text.match(/\n/g) || []).length*15 +30
      width: root.width
      spacing: 0
      Rectangle {
        color: "transparent"
        height: parent.height
        width: parent.width/100*10
        border.width: 1
        border.color: "black"
        Text { text: peername; anchors.fill: parent; anchors.margins: 4 }
      }
      Rectangle {
        color: "transparent"
        height: parent.height
        width: parent.width/100*16
        border.width: 1
        border.color: "black"
        Text { text: name; anchors.fill: parent; anchors.margins: 4 }
      }
      Rectangle {
        color: "transparent"
        height: parent.height
        width: parent.width/100*16
        border.width: 1
        border.color: "black"
        Text { text: modifiers; anchors.fill: parent; anchors.margins: 4 }
      }
      Rectangle {
        color: "transparent"
        height: parent.height
        width: parent.width/100*7
        border.width: 1
        border.color: "black"
        Text { text: valuetype; anchors.fill: parent; anchors.margins: 4 }
      }
      Rectangle {
        color: "transparent"
        height: parent.height
        width: parent.width/100*3
        border.width: 1
        border.color: "black"
        Text {
          function getParameterCount(par1) {
            if(par1 && par1.split)
            {
              if(String(par1).indexOf(";") > -1)
              {
                return par1.split(";").length
              }
            }
            else if(par1 && par1.length)
            {
              return par1.length;
            }
            else
              return "";
          }

          property var tmpText: getParameterCount(value)

          text: tmpText ? tmpText : ""
          anchors.fill: parent;
          anchors.margins: 4
        }
      }
      Rectangle {
        color: "transparent"
        height: parent.height
        width: root.width/100*48
        border.width: 1
        border.color: "black"
        //Text { text: String(value); anchors.fill: parent; anchors.margins: 4 }

        Rectangle {
          id: hotBar
          visible: heatMap.checked
          onVisibleChanged: {
            colorRed=0;
            colorBlue=1;
          }

          color: Qt.rgba(colorRed, 0.3, colorBlue, 0.3+(colorRed*0.1))
          Behavior on color {
            enabled: visible
            ColorAnimation {
              duration: 10
            }
          }
          property real colorRed: 0
          property real colorBlue: 1

          anchors.fill: parent
          Timer {
            interval: 20
            running: parent.visible
            repeat: true
            onTriggered: {
              parent.colorBlue+=0.003
              parent.colorRed-=0.003

              parent.colorBlue = Math.min(1, parent.colorBlue)
              parent.colorRed = Math.max(0, parent.colorRed)
            }
          }
        }

        TextEdit {
          id: tEdit
          text: String(value);
          onTextChanged: {
            hotBar.colorRed+=0.05
            //hotBar.colorBlue-=0.5;
            hotBar.colorBlue=0

            hotBar.colorRed = Math.min(1, hotBar.colorRed)
            hotBar.colorBlue = Math.max(0, hotBar.colorBlue)
          }
          width: parent.width-30
          anchors.top: parent.top
          anchors.bottom: parent.bottom
          anchors.left: parent.left
          anchors.margins: 4;
          //textFormat: TextEdit.RichText
        }
        Button {
          width: height
          anchors.right: parent.right
          anchors.top: parent.top
          anchors.left: tEdit.right
          anchors.margins: 4;
          onClicked: {
            console.log(entModel.src+peername+"#"+name)
          }
        }
      }
    }
  }
}

#include <QApplication>
#include <QQmlApplicationEngine>

#include <qqml.h>
#include <QList>
#include <QDebug>
#include <QDataStream>
#include <QSessionManager>
#include <QPixmap>
#include <QIcon>

int main(int argc, char *argv[])
{
  qRegisterMetaTypeStreamOperators<QList<double> >("QList<double>");
  qRegisterMetaTypeStreamOperators<QList<int> >("QList<int>");
  qRegisterMetaTypeStreamOperators<QList<QString> >("QList<QString>");

  QApplication app(argc, argv);

  QQmlApplicationEngine engine;
  engine.load(QUrl(QStringLiteral("qrc:///qml/libvein-debugger/main.qml")));

  return app.exec();
}

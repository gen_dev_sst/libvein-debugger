# Add more folders to ship with the application, here
folder_01.source = qml/libvein-debugger
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01


exists( ../include/project-paths.pri ) {
  include(../include/project-paths.pri)
}
else:exists( ../../project-paths.pri ) {
  include(../../project-paths.pri)
} else:exists(../gui-session.pri) {
  include(../gui-session.pri)
}

LIBS += $$ANDROID_PROTOBUF_LIBDIR -lprotobuf

INCLUDEPATH += $$VEIN_INCLUDEDIR
LIBS += $$VEIN_LIBDIR -lvein-qt
INCLUDEPATH += $$PROTONET_INCLUDEDIR
LIBS += $$PROTONET_LIBDIR -lproto-net-qt
INCLUDEPATH += $$VEIN_PROTOBUF_INLCUDEDIR
LIBS += $$VEIN_PROTOBUF_LIBDIR -lvein-qt-protobuf
INCLUDEPATH += $$VEIN_TCP_INCLUDEDIR
LIBS += $$VEIN_TCP_LIBDIR -lvein-tcp-overlay

LIBS += $$QWT_LIBDIR -lqwt

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

SOURCES += main.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

ANDROID_EXTRA_LIBS = /home/samuel/tmp/android-protobuf/android_libs/protobuf/obj/local/armeabi-v7a/libgnustl_shared.so \
  $$ANDROID_PROTOBUF_LIB \
  $$PROTONET_LIB \
  $$VEIN_LIB \
  $$VEIN_TCP_LIB \
  $$VEIN_QML_LIB \
  $$QWT_LIB \
  $$QWT_QML_LIB

OTHER_FILES += \
    ../build-libvein-qml-Android_für_armeabi_v7a_GCC_4_8_Qt_5_2_1-Debug/qmldir \
  /work/qt_projects/build-QmlQwtPlot-Android_für_armeabi_v7a_GCC_4_8_Qt_5_2_1-Debug/qmldir


# - setup the correct location to install to and load from
android {
    # android platform
    # From: http://community.kde.org/Necessitas/Assets
    PLUGINS_INSTALL_PATH=/assets/import
} else {
    # other platforms
    PLUGINS_INSTALL_PATH=$$OUT_PWD/import
}

QT += opengl
# - setup the 'make install' step
plugins.path = $$PLUGINS_INSTALL_PATH
plugins.files += ../build-libvein-qml-Android_für_armeabi_v7a_GCC_4_8_Qt_5_2_1-Debug/qmldir
plugins.files += /work/qt_projects/build-QmlQwtPlot-Android_für_armeabi_v7a_GCC_4_8_Qt_5_2_1-Debug/qmldir
plugins.depends += FORCE

INSTALLS += plugins

RESOURCES += \
    data.qrc
